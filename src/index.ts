#!/usr/bin/env node
import { Main } from "./Main";
import { Options } from "./Options";
import chalk from "chalk";
import clear from "clear";
import figlet from "figlet";
import program from "commander";

clear();
console.log(
	chalk.blue(figlet.textSync("nightly", { horizontalLayout: "full" }))
);
program
	.version("1.0.0")
	.description("Merge and push pending MR/PR into a nightly branch")
	.requiredOption("-u --username <username>", "bitbucket username")
	.requiredOption("-p --password <password>", "bitbucket password")
	.requiredOption("-o --owner <owner>", "repo owner")
	.requiredOption("-r --repo <repo>", "repo")
	.option(
		"-i --include <include>",
		"Only include branches with MR with matching titles"
	)
	.option(
		"-e --exclude <exclude>",
		"Don't include branches with MR with matching titles"
	)
	.option("-d, --dry", "Don't push changes")
	.arguments("directory")
	.parse(process.argv);

if (program.args.length !== 1) {
	program.outputHelp();
}

const options = new Options({
	directory: program.args[0],
	dry: program.dry as boolean,
	exclude: program.exclude as string,
	include: program.include as string,
	owner: program.owner as string,
	password: program.password as string,
	repo: program.repo as string,
	username: program.username as string,
});

void new Main({ options }).run();
