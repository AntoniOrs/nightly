import { Bitbucket } from "./backends/bitbucket/Bitbucket";
import { Git } from "./utils/Git";
import { IBackend } from "./backends/Backend";
import { IMergeRequest } from "./backends/MergeRequest";
import { Options } from "./Options";
import chalk from "chalk";

export class Main {
	private options: Options;
	private git: Git;
	constructor({ options }: { options: Options }) {
		this.options = options;
		this.git = new Git({ direcotory: this.options.directory });
	}

	public async run(): Promise<void> {
		try {
			await this.rebaseBranch();
			const branches: Array<string> = await this.getBranches();
			await this.checkoutBranches(branches);
			await this.mergeBranches(branches);
			await this.pushChanges();
		} catch (error) {
			console.error(chalk.red(JSON.stringify(error)));
		}
	}

	private async getBranches(): Promise<Array<string>> {
		console.log("2. Getting branches");
		const backend: IBackend = new Bitbucket({ options: this.options });
		const mergeRequests: Array<IMergeRequest> = await backend.getBranches();
		const filteredMergeRequests: Array<IMergeRequest> = mergeRequests
			.filter((mergeRequest) =>
				this.options.include
					? this.options.include.test(mergeRequest.title)
					: true
			)
			.filter((mergeRequest) =>
				this.options.exclude
					? !this.options.exclude.test(mergeRequest.title)
					: true
			);
		console.log(
			`2. Found ${filteredMergeRequests.length} ${
				filteredMergeRequests.length === 1 ? "branch" : "branches"
			}.`
		);
		const branches = filteredMergeRequests.map(
			(mergeRequest) => mergeRequest.branch
		);
		return branches;
	}

	private async rebaseBranch(): Promise<void> {
		console.log(`1. Reseting`);
		let result = await this.git.runCommand("reset", "--hard");
		console.log(`1. ${result.trim()}`);
		console.log(`1. Fetching changes`);
		await this.git.runCommand("fetch", "-pP");
		result = await this.git.runCommand("checkout", "develop");
		console.log(`1. ${result.trim()}`);
		await this.git.runCommand("pull");
		await this.git.runCommand("checkout", "-B", "nightly");
	}

	private async checkoutBranches(branches: Array<string>) {
		let result: string;
		for (const branch of branches) {
			result = await this.git.runCommand("checkout", branch);
			console.log(`3. ${result.trim()}`);
		}
		await this.git.runCommand("checkout", "nightly");
	}

	private async mergeBranches(branches: Array<string>) {
		for (const branch of branches) {
			try {
				await this.git.runCommand("merge", branch);
				console.log(`4. Merged ${branch}`);
			} catch (e) {
				await this.git.runCommand("merge", "--abort");
				console.log(chalk.red(`4. Failed to merge ${branch}`));
			}
		}
	}

	private async pushChanges() {
		if (this.options.dry) {
			console.log(`5. I would push to origin/nightly`);
		} else {
			const result = await this.git.runCommand(
				"push",
				"-f",
				"--set-upstream",
				"origin",
				"nightly"
			);
			console.log(`5. ${result.trim()}`);
		}
	}
}
