export interface IBitbucketResponse {
	page: number;
	pagelen: number;
	size: number;
	values: Array<IValue>;
}

export interface IValue {
	author: IAuthor;
	close_source_branch: boolean;
	closed_by: null;
	comment_count: number;
	created_on: Date;
	description: string;
	destination: IDestination;
	id: number;
	links: { [key: string]: ILink };
	merge_commit: null;
	reason: string;
	source: IDestination;
	state: string;
	summary: ISummary;
	task_count: number;
	title: string;
	type: string;
	updated_on: Date;
}

export interface IAuthor {
	account_id: string;
	display_name: string;
	links: IAuthorLinks;
	nickname: string;
	type: string;
	uuid: string;
}

export interface IAuthorLinks {
	avatar: ILink;
	html: ILink;
	self: ILink;
}

export interface ILink {
	href: string;
}

export interface IDestination {
	branch: IBranch;
	commit: ICommit;
	repository: IRepository;
}

export interface IBranch {
	name: string;
}

export interface ICommit {
	hash: string;
	links: ICommitLinks;
	type: string;
}

export interface ICommitLinks {
	html: ILink;
	self: ILink;
}

export interface IRepository {
	full_name: string;
	links: IAuthorLinks;
	name: string;
	type: string;
	uuid: string;
}

export interface ISummary {
	html: string;
	markup: string;
	raw: string;
	type: string;
}
