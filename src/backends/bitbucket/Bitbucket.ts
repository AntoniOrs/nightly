import { IBackend } from "../Backend";
import { IBitbucketResponse } from "./BitbucketResponse";
import { IMergeRequest } from "../MergeRequest";
import { Options } from "../../Options";
import axios from "axios";

export class Bitbucket implements IBackend {
	private options: Options;

	constructor({ options }: { options: Options }) {
		this.options = options;
	}

	public async getBranches(): Promise<Array<IMergeRequest>> {
		const output: Array<IMergeRequest> = [];
		const url = `https://api.bitbucket.org/2.0/repositories/${this.options.owner}/${this.options.repo}/pullrequests`;
		const axiosResponse = await axios.get<IBitbucketResponse>(url, {
			auth: {
				password: this.options.password,
				username: this.options.username,
			},
			responseType: "json",
		});
		for (const value of axiosResponse.data.values) {
			output.push({
				branch: value.source.branch.name,
				title: value.title,
			});
		}
		return output;
	}
}
