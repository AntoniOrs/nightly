import { IMergeRequest } from "./MergeRequest";

export interface IBackend {
	getBranches(): Promise<Array<IMergeRequest>>;
}
