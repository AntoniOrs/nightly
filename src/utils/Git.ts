import { runCommand } from "./runCommand";

export class Git {
	private direcotory: string;
	constructor({ direcotory }: { direcotory: string }) {
		this.direcotory = direcotory;
	}

	public async runCommand(...command: Array<string>): Promise<string> {
		return runCommand(["git", ...command].join(" "), this.direcotory);
	}
}
